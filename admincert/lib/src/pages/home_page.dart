import 'package:flutter/material.dart';
import 'package:admincert/src/blocs/provider.dart';
import 'package:admincert/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:admincert/src/widgets/menu_drawer.dart';


class HomePage extends StatefulWidget {
  static final String routeName = 'home';
  
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _prefs = new PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);

    print('==================');
    print('Usuario => ${bloc.usuario}');
    print('Contraseña => ${bloc.contrasena}');
    print('==================');

    return new Scaffold(
      drawer: MenuDrawer(),
      //backgroundColor: Color(0xffF0DF87),
      appBar: _buildAppBar(),
      body: _profile(),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
        //backgroundColor: Color(0xfffbdd00),
        title: Text(
          'Datos personales',
          style:
              TextStyle(color: Colors.white), //, fontWeight: FontWeight.w500),
        ),
        centerTitle: true,
        /*actions: <Widget>[
          FlatButton(
            child: Text('Edit',
                style: TextStyle(color: Colors.white, fontSize: 15.0)),
            onPressed: () {},
          ),
        ]*/
      );
  }

  Widget _profile() {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: ListView(padding: EdgeInsets.all(10.0), children: <Widget>[
        SizedBox(height: 15.0),
        CircleAvatar(
          radius: 65.0,
          backgroundColor: Colors.blue,
          child: Text(
            _prefs.nombre[0],
            style: TextStyle(fontSize: 40.0),
          ),
        ),
        SizedBox(height: 25.0),
        Center(child: Text('Información')),
        SizedBox(height: 25.0),
        Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
          Text("Nombres:   ", style: TextStyle(color: Colors.black)),
          Text(_prefs.nombre, style: TextStyle(color: Colors.black45, fontSize: 15.0)),
        ]),
        SizedBox(height: 15.0),
        Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
          Text("Apellidos:   ", style: TextStyle(color: Colors.black)),
          Text(_prefs.apellido, style: TextStyle(color: Colors.black45, fontSize: 15.0)),
        ]),
        SizedBox(height: 15.0),
        Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
          Text("DNI:   ", style: TextStyle(color: Colors.black)),
          Text(_prefs.dni, style: TextStyle(color: Colors.black45, fontSize: 15.0)),
        ]),
        SizedBox(height: 15.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
          Text("Correo:   ", style: TextStyle(color: Colors.black)),
          Text(_prefs.correo, style: TextStyle(color: Colors.black45, fontSize: 15.0)),
        ]),
        SizedBox(height: 15.0),
        Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
          Text("Dirección:   ", style: TextStyle(color: Colors.black)),
          Text(_prefs.direccion, style: TextStyle(color: Colors.black45, fontSize: 15.0)),
        ]),
        SizedBox(height: 15.0),
        Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
          Text("Telefono:   ", style: TextStyle(color: Colors.black)),
          Text(_prefs.telefono, style: TextStyle(color: Colors.black45, fontSize: 15.0)),
        ]),
        SizedBox(height: 15.0),
        Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
          Text("Fecha de nacimiento:   ", style: TextStyle(color: Colors.black)),
          Text(_prefs.fechaNacimiento, style: TextStyle(color: Colors.black45, fontSize: 15.0)),
        ]),
      ]),
    );
  }
}
