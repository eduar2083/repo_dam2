import 'package:admincert/src/dto/alumno_asignatura_dto.dart';
import 'package:admincert/src/pages/lista_notas_registro_page.dart';
import 'package:flutter/material.dart';
import 'package:admincert/src/models/asignatura_persona_model.dart';
import 'package:admincert/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:admincert/src/providers/alumno_asignatura_provider.dart';

class ListaAlumnoPage extends StatelessWidget {
  static final routeName = 'lista_alumnos';
  final _provider = new AlumnoAsignaturaProvider();
  final _prefs = new PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    final AsignaturaPersonaModel asignatura = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text('Alumnos - ${asignatura.asignatura}', style: TextStyle(fontSize: 14),),
        centerTitle: true,
      ),
      body: _listaAlumnos(asignatura), 
    );
  }

  Widget _listaAlumnos(AsignaturaPersonaModel asignatura) {
    return FutureBuilder(
      future: _provider.listar(asignatura.idasignatura, _prefs.idPersona),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        return ListView(
          children: _buildItems(context, snapshot.data, asignatura),
        );
      }
    );
  }

  List<Widget> _buildItems(BuildContext context, List<dynamic> lista, AsignaturaPersonaModel asignatura) {
    return lista.map((e) {
      return Column(
        children: <Widget>[
          ListTile(
            title: Text('${e.nombre}, ${e.apellido}'),
            leading: Icon(Icons.perm_identity, color: Colors.deepPurple,),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () {
              final dto = new AlumnoAsignaturaDto(e, asignatura);
              // dto.alumnoAsignatura = e;
              Navigator.pushNamed(context, ListaNotasRegistroPage.routeName, arguments: dto);
            },
          ),
          Divider(),
        ],
      );
    }).toList();
  }
}
