import 'package:admincert/src/pages/home_page.dart';
import 'package:admincert/src/providers/login_provider.dart';
import 'package:admincert/src/utils/utils.dart' as util;
import 'package:flutter/material.dart';
import 'package:admincert/src/blocs/provider.dart';

class LoginPage extends StatefulWidget {
  static final String routeName = "/";

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final LoginProvider provider = new LoginProvider();

  bool _contrasenaVisible;

  @override
  void initState() {
    super.initState();
    _contrasenaVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _crearFondo(context),
          _loginForm(context)
        ],
      ),
    );
  }

  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final fondo = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Color.fromRGBO(63, 63, 156, 1.0),
        Color.fromRGBO(90, 70, 178, 1.0)
      ])),
    );

    final circulo = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          color: Color.fromRGBO(255, 255, 255, 0.05)),
    );

    return Stack(
      children: <Widget>[
        fondo,
        Positioned(top: 90.0, left: 30.0, child: circulo),
        Positioned(top: -40.0, left: -30.0, child: circulo),
        Positioned(bottom: -50.0, right: -10.0, child: circulo),
        Positioned(bottom: 120.0, right: 20.0, child: circulo),
        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Icon(
                Icons.person_pin_circle,
                color: Colors.white,
                size: 100.0,
              ),
              SizedBox(
                height: 10.0,
                width: double.infinity,
              ),
              Text('AdminCert',
                  style: TextStyle(color: Colors.white, fontSize: 25.0))
            ],
          ),
        )
      ],
    );
  }

  Widget _loginForm(BuildContext context) {
    final bloc = Provider.of(context);

    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
            child: Container(
              height: 180.0,
            ),
          ),
          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 50.0),
            padding: EdgeInsets.symmetric(vertical: 50.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black26,
                      blurRadius: 3.0,
                      offset: Offset(0.0, 5.0),
                      spreadRadius: 3.0)
                ]),
            child: Column(
              children: <Widget>[
                Text('Ingreso', style: TextStyle(fontSize: 20.0)),
                SizedBox(height: 60.0),
                _crearUsuario(bloc),
                SizedBox(height: 30.0),
                _crearContrasena(bloc),
                SizedBox(height: 30.0),
                _crearBoton(bloc)
              ],
            ),
          ),
          //Text('¿Olvidaste tu contraseña?'),
          SizedBox(
            height: 100.0,
          )
        ],
      ),
    );
  }

  Widget _crearUsuario(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.usuarioStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            style: TextStyle(
              fontSize: 19.0,
            ),
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              icon: Icon(Icons.account_circle, color: Colors.deepPurple),
              hintText: 'Usuario',
              labelText: 'Usuario',
              //counterText: snapshot.data,
              errorText: snapshot.error,
            ),
            onChanged: bloc.changeUsuario,
          ),
        );
      },
    );
  }

  Widget _crearContrasena(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.contrasenaStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            obscureText: !_contrasenaVisible,
            decoration: InputDecoration(
                icon: Icon(
                  Icons.lock_outline,
                  color: Colors.deepPurple,
                ),
                hintText: 'Contraseña',
                labelText: 'Contraseña',
                //counterText: snapshot.data,
                errorText: snapshot.error,
                suffixIcon: GestureDetector(
                  child: Icon(_contrasenaVisible ? Icons.visibility : Icons.visibility_off),
                  onTap: () {
                    setState(() {
                      _contrasenaVisible = !_contrasenaVisible;
                    });
                  },
                )
              ),
            onChanged: bloc.changeContrasena,
          ),
        );
      },
    );
  }

  Widget _crearBoton(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (context, snapshot) {
        return RaisedButton(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
            child: Text('Ingresar'),
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          elevation: 0.0,
          color: Colors.deepPurple,
          textColor: Colors.white,
          onPressed: snapshot.hasData ? () => _login(bloc, context) : null,
        );
      },
    );
  }

  _login(LoginBloc bloc, BuildContext context) async {
    /*print('==================');
    print('Usuario: ${bloc.usuario}');
    print('Contraseña: ${bloc.contrasena}');
    print('==================');*/

    try {
      final u = await provider.autenticarUsuario(bloc.usuario, bloc.contrasena);

      if (u != null) {
        Navigator.pushReplacementNamed(context, HomePage.routeName);
      } else {
        print('Credenciales incorrectas');
        util.mostrarAlerta(context, 'Error', 'Credenciales incorrectas');
      }
    }
    catch (e) {
      util.mostrarMensajeError(context);
    }
  }
}
