import 'package:flutter/material.dart';
import 'package:admincert/src/models/asignatura_persona_model.dart';
import 'package:admincert/src/pages/lista_notas_page.dart';
import 'package:admincert/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:admincert/src/providers/asignatura_persona_provider.dart';
import 'package:admincert/src/widgets/menu_drawer.dart';
import 'package:admincert/src/utils/utils.dart' as util;

class ListaCursosAlumnoPage extends StatelessWidget {
  static final String routeName = 'lista_cursos_alumno';
  final _prefs = new PreferenciasUsuario();
  final _provider = new AsignaturaPersonaProvider();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      drawer: MenuDrawer(),
      appBar: AppBar(
        title: Text("Cursos disponibles"),
        centerTitle: true,
      ),
      body: _misCursos(context, _prefs),
    );
  }

  Widget _misCursos(BuildContext context, PreferenciasUsuario prefs) {
    try {
      return FutureBuilder(
        future: _provider.listar(_prefs.idPersona),
        initialData: [],
        builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
          List<ListTile> items = _buildAsignaturas(context, snapshot.data);

          return Column(
            children: [
              Row(children: [
                Expanded(
                    child: Text('     Asignaturas',
                        style: TextStyle(
                          height: 2.0,
                          fontSize: 15.2,
                          fontWeight: FontWeight.bold,
                        ))),
                Expanded(
                    child: Text('                              Notas',
                        style: TextStyle(
                          height: 2.0,
                          fontSize: 15.2,
                          fontWeight: FontWeight.bold,
                        ))),
              ]),
              Expanded(
                child: ListView(children: items),
              ),
            ],
          );
        },
      );
    } catch (e) {
      util.mostrarMensajeError(context);
    }
  }

  List<ListTile> _buildAsignaturas(
      BuildContext context, List<dynamic> asignaturas) {
    List<ListTile> lista = new List();

    for (AsignaturaPersonaModel e in asignaturas) {
      lista.add(ListTile(
        title: Text(e.asignatura),
        trailing: Icon(Icons.keyboard_arrow_right),
        onTap: () {
          Navigator.pushNamed(context, ListaNotasPage.routeName, arguments: e);
        },
      ));
    }

    return lista;
  }
}
