import 'package:admincert/src/dto/nota_dto.dart';
import 'package:flutter/material.dart';
import 'package:admincert/src/providers/nota_provider.dart';
import 'package:admincert/src/dto/alumno_asignatura_dto.dart';
import 'package:admincert/src/providers/nota_asignatura_provider.dart';
import 'package:admincert/src/utils/utils.dart' as utils;
import 'package:admincert/src/models/nota_model.dart';

class ListaNotasRegistroPage extends StatefulWidget {
  static final routeName = 'lista_notas_registro';

  @override
  _ListaNotasRegistroPageState createState() => _ListaNotasRegistroPageState();
}

class _ListaNotasRegistroPageState extends State<ListaNotasRegistroPage> {
  final _formKey = GlobalKey<FormState>();

  final _notaAsignaturaProvider = new NotaAsignaturaProvider();
  final _notaProvider = new NotaAProvider();

  TextEditingController controller = TextEditingController();
  List<NotaModel> listaFinal = new List();
  AlumnoAsignaturaDto _dto;

  bool _desabilitaSubmit;

  @override
  void initState() {
    super.initState();
    setState(() {
      _desabilitaSubmit = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    _dto = ModalRoute.of(context).settings.arguments;

    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Notas - ${_dto.asignaturaPersona.asignatura}',
              style: TextStyle(fontSize: 14)),
          centerTitle: true,
        ),
        body: Stack(
          children: <Widget>[
            _resultadosNotas(context),
          ],
        ));
  }

  Widget _resultadosNotas(BuildContext context) {
    return new Container(
        padding: EdgeInsets.all(16.0),
        child: new Form(
          key: _formKey,
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              _mostrarNotasActuales(),
              SizedBox(
                height: 50.0,
              ),
              _crearBoton(context)
            ],
          ),
        ));
  }

  Widget _mostrarNotasActuales() {
    return FutureBuilder(
      future: _notaAsignaturaProvider.listar(
          _dto.asignaturaPersona.idasignatura, _dto.alumnoAsignatura.idpersona),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        final items = _buildNotas(snapshot.data);

        return DataTable(columns: const <DataColumn>[
          DataColumn(
            label: Text(
              "Tipo evaluación",
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
          DataColumn(
            label: Text(
              "  Nota",
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        ], rows: items);
      },
    );
  }

  List<DataRow> _buildNotas(List<dynamic> notas) {
    List<DataRow> lista = new List();

    for (var e in notas) {
      lista.add(DataRow(cells: <DataCell>[
        DataCell(Text(
          e.tipoevaluacion,
          style: TextStyle(fontSize: 18.0),
        )), //, textAlign: TextAlign.center
        DataCell(
          Container(
            padding: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
            child: TextFormField(
              initialValue: (e.nota < 0 ? '' : e.nota.toString()),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18.0,
              ),
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5)),
                  errorStyle: TextStyle(fontSize: 0)
                  //hintText: 'Nombre de la persona',
                  //labelText: 'Nombre',
                  //helperText: 'Solo letras',
                  //suffixIcon: Icon(Icons.accessibility),
                  //icon: Icon(Icons.account_circle)),
                  //onChanged: (valor) => _nombreChanged(valor),
                  ),
              validator: (value) {
                if (value == '') return null;

                if (utils.isNumeric(value)) {
                  if (value.length > 5) {
                    return '';
                  }

                  double nota = double.parse(value);
                  if (nota <= 20) return null;
                  return '';
                } else {
                  return '';
                }
              },
              onSaved: (value) {
                if (utils.isNumeric(value)) {
                  e.nota = double.parse(value);
                  listaFinal.add(e);
                }
              },
            ),
          ),
        )
      ]));
    }

    return lista;
  }

  Widget _crearBoton(BuildContext context) {
    return RaisedButton(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
        child: Text('Grabar'),
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      elevation: 0.0,
      color: Colors.deepPurple,
      textColor: Colors.white,
      onPressed: _desabilitaSubmit ? null : _submit,
    );
  }

  void _submit() async {
    if (!_formKey.currentState.validate()) {
      utils.mostrarAlerta(context, 'Error', 'Dato incorrectos');
      return;
    }

    listaFinal.clear();
    _formKey.currentState.save();

    if (listaFinal.length > 0) {
      bool r;

      _toggleSubmit();

      for (var n in listaFinal) {
        final nota = new NotaDto(
            n.idnota, _dto.alumnoAsignatura.idpersona, n.idevaluacion, n.nota);
        if (n.idnota > 0) {
          r = await _notaProvider.actualizar(nota);
        } else {
          r = await _notaProvider.registrar(nota);
        }
      }

      // Redibujar listado
      setState(() {
        _mostrarNotasActuales();
        _toggleSubmit();
      });

      if (r == true) {
        utils.mostrarAlerta(context, 'Éxito', 'Se han grabado correctamente!');
      }
      else {
        utils.mostrarAlerta(context, 'Error', 'Ha ocurrido un error');
      }
    }
    else {
      utils.mostrarAlerta(context, 'Advertencia', 'No se ha guardado ninguna nota');
    } 
  }

  void _toggleSubmit() {
    setState(() {
      _desabilitaSubmit = !_desabilitaSubmit;
    });
  }
}
