import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/cupertino.dart';
//import 'package:admincert/src/utils/utils.dart';
import 'package:admincert/src/pages/login_page.dart';

class RegistroUsuarioPage extends StatefulWidget {
  @override
  _RegistroState createState() => _RegistroState();
}

class _RegistroState extends State<RegistroUsuarioPage> {
  String _dni = '';
  String _nombres = '';
  String _apellidos = '';
  String _fechaNacimiento = '';
  String _telefono = '';
  String _email = '';
  String _usuario = '';
  String _password;

  String _opcionSeleccionada = 'Volar';

  List<String> _poderes = ['Volar', 'Rayos X', 'Super Aliento', 'Super Fuerza'];

  TextEditingController _inputFieldDateController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Registro de usuario'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
        children: <Widget>[
          _crearInputDni(),
          Divider(),
          _crearInputNombres(),
          Divider(),
          _crearInputApellidos(),
          Divider(),
          _crearDatePicker(context),
          Divider(),
          _crearInputTelefono(),
          Divider(),
          _crearEmail(),
          Divider(),
          _crearInputUsuario(),
          Divider(),
          _crearPassword(),
          Divider(),
          _crearBotonEnviarDatos()
        ],
      ),
    );
  }

  Widget _crearInputDni() {
    return TextField(
      autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text('Dígitos ${_dni.length}'),
          hintText: 'Número de dni',
          labelText: 'Dni',
          icon: Icon(Icons.account_circle)),
      onChanged: (valor) {
        setState(() {
          _dni = valor;
        });
      },
    );
  }

  Widget _crearInputNombres() {
    return TextField(
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text('Letras ${_nombres.length}'),
          hintText: 'Nombres',
          labelText: 'Nombres',
          icon: Icon(Icons.account_circle)),
      onChanged: (valor) {
        setState(() {
          _nombres = valor;
        });
      },
    );
  }

  Widget _crearInputApellidos() {
    return TextField(
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text('Letras ${_apellidos.length}'),
          hintText: 'Apellidos',
          labelText: 'Apellidos',
          icon: Icon(Icons.account_circle)),
      onChanged: (valor) {
        setState(() {
          _apellidos = valor;
        });
      },
    );
  }

  Widget _crearDatePicker(BuildContext context) {
    return TextField(
      enableInteractiveSelection: false,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          hintText: 'Fecha de nacimiento',
          labelText: 'Fecha de nacimiento',
          suffixIcon: Icon(Icons.perm_contact_calendar),
          icon: Icon(Icons.calendar_today)),
      onTap: () => _selectorFecha(context),
    );
  }

  Widget _crearInputTelefono() {
    return TextField(
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text('Letras ${_telefono.length}'),
          hintText: 'Teléfono',
          labelText: 'Teléfono',
          helperText: 'Fijo o móvil',
          icon: Icon(Icons.phone)),
      onChanged: (valor) {
        setState(() {
          _telefono = valor;
        });
      },
    );
  }

  Widget _crearEmail() {
    return TextField(
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
            hintText: 'Email',
            labelText: 'Email',
            suffixIcon: Icon(Icons.alternate_email),
            icon: Icon(Icons.email)),
        onChanged: (valor) => setState(() {
              _email = valor;
            }));
  }

  Widget _crearInputUsuario() {
    return TextField(
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          counter: Text('Letras ${_usuario.length}'),
          hintText: 'Usuario',
          labelText: 'Usuario',
          icon: Icon(Icons.account_circle)),
      onChanged: (valor) {
        setState(() {
          _password = valor;
        });
      },
    );
  }

  Widget _crearPassword() {
    return TextField(
        obscureText: true,
        decoration: InputDecoration(
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
            hintText: 'Password',
            labelText: 'Password',
            suffixIcon: Icon(Icons.lock_open),
            icon: Icon(Icons.lock)),
        onChanged: (valor) => setState(() {
              _email = valor;
            }));
  }

  _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime.now(),
        firstDate: new DateTime(2018),
        lastDate: new DateTime(2025),
        locale: Locale('es', 'ES'));

    if (picked != null) {
      setState(() {
        _fechaNacimiento = picked.toString();
        _inputFieldDateController.text = _fechaNacimiento;
      });
    }
  }

  Widget _crearBotonEnviarDatos() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
              elevation: 5.0,
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0)),
              color: Colors.blue,
              child: new Text('Enviar datos',
                  style: new TextStyle(fontSize: 20.0, color: Colors.white)),
              onPressed: _mostrarMensaje ////
              ),
        ));
  }

  _selectorFecha(BuildContext context) async {
    FocusScope.of(context).requestFocus(new FocusNode());

    DateTime date = await showDatePicker(
        context: context,
        initialDate: new DateTime(2002),
        firstDate: new DateTime(1900),
        lastDate: new DateTime(2002));
  }

  void _mostrarMensaje() {
    //mostrarAlerta(context, 'Registrado', 'Se registró correctamente');
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(
              "Bienvenido",
              textAlign: TextAlign.center,
            ),
            content: new Text("Se ha registrado correctamente"),
            actions: <Widget>[
              new FlatButton(
                  onPressed: () {
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (context) => LoginPage()),
                        (Route<dynamic> route) => false);
                  },
                  child: new Text("Iniciar sesión")),
            ],
          );
        });
  }
}
