import 'package:flutter/material.dart';
import 'package:admincert/src/models/asignatura_persona_model.dart';
import 'package:admincert/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:admincert/src/providers/nota_asignatura_provider.dart';

class ListaNotasPage extends StatefulWidget {
  static final routeName = 'lista_notas';

  ListaNotasPage({Key key}) : super(key: key);

  @override
  _ListaNotasPageState createState() => _ListaNotasPageState();
}

class _ListaNotasPageState extends State<ListaNotasPage> {
  final _provider = new NotaAsignaturaProvider();
  final _prefs = new PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    final AsignaturaPersonaModel asignatura =
        ModalRoute.of(context).settings.arguments;

    print('Args=${asignatura.idasignatura} - ${asignatura.asignatura}');

    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Notas - ${asignatura.asignatura}', style: TextStyle(fontSize: 14)),
          centerTitle: true,
        ),
        body: Stack(
          children: <Widget>[
            _resultadosNotas(asignatura),
          ],
        ));
  }

  Widget _resultadosNotas(AsignaturaPersonaModel asignatura) {
    return new Container(
        padding: EdgeInsets.all(16.0),
        child: new Form(
          // key: _formKey,
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              mostrarNotasActuales(asignatura),
            ],
          ),
        ));
  }

  Widget mostrarNotasActuales(AsignaturaPersonaModel asignatura) {
    return FutureBuilder(
      future: _provider.listar(asignatura.idasignatura, _prefs.idPersona),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        final items = _buildNotas(snapshot.data);

        return DataTable(columns: const <DataColumn>[
          DataColumn(
            label: Text(
              "Tipo evaluación",
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
          DataColumn(
            label: Text(
              "          ",
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
          DataColumn(
            label: Text(
              "Nota",
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        ], 
        rows: items
        );
      },
    );
  }

  List<DataRow> _buildNotas(List<dynamic> notas) {
    List<DataRow> lista = new List();

    for (var e in notas) {
      lista.add(DataRow(cells: <DataCell>[
        DataCell(Text(e.tipoevaluacion)), //, textAlign: TextAlign.center
        DataCell(Text(" ")),
        DataCell(Text(e.nota < 0 ?  '' : e.nota.toString())),
      ]));
    }

    return lista;
  }
}
