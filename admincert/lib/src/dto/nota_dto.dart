class NotaDto {
  final int idnota;
  final int idpersona;
  final int idevaluacion;
  final double nota;

  NotaDto(this.idnota, this.idpersona, this.idevaluacion, this.nota);

  Map<String, dynamic> toJson() => {
      "idnota": idnota,
      "idpersona": idpersona,
      "idevaluacion": idevaluacion,
      "nota": nota
  };
}