import 'package:admincert/src/models/alumno_asignatura_model.dart';
import 'package:admincert/src/models/asignatura_persona_model.dart';

class AlumnoAsignaturaDto {
  final AlumnoAsignaturaModel alumnoAsignatura;
  final AsignaturaPersonaModel asignaturaPersona;

  AlumnoAsignaturaDto(this.alumnoAsignatura, this.asignaturaPersona);
}