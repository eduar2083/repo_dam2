// To parse this JSON data, do
//
//     final enlaceModel = enlaceModelFromJson(jsonString);

import 'dart:convert';

List<EnlaceModel> enlaceModelFromJson(String str) => List<EnlaceModel>.from(json.decode(str).map((x) => EnlaceModel.fromJson(x)));

String enlaceModelToJson(List<EnlaceModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class EnlaceModel {
    EnlaceModel({
        this.idenlace,
        this.descripcion,
        this.ruta,
    });

    int idenlace;
    String descripcion;
    String ruta;

    factory EnlaceModel.fromJson(Map<String, dynamic> json) => EnlaceModel(
        idenlace: json["idenlace"],
        descripcion: json["descripcion"],
        ruta: json["ruta"],
    );

    Map<String, dynamic> toJson() => {
        "idenlace": idenlace,
        "descripcion": descripcion,
        "ruta": ruta,
    };
}
