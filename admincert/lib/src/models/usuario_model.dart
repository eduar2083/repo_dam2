// To parse this JSON data, do
//
//     final usuarioModel = usuarioModelFromJson(jsonString);

import 'dart:convert';

UsuarioModel usuarioModelFromJson(String str) => UsuarioModel.fromJson(json.decode(str));

String usuarioModelToJson(UsuarioModel data) => json.encode(data.toJson());

class UsuarioModel {
    UsuarioModel({
        this.idusuario,
        this.login,
        this.clave,
        this.correo,
        this.idpersona,
    });

    int idusuario;
    String login;
    String clave;
    String correo;
    int idpersona;

    factory UsuarioModel.fromJson(Map<String, dynamic> json) => UsuarioModel(
        idusuario: json["idusuario"],
        login: json["login"],
        clave: json["clave"],
        correo: json["correo"],
        idpersona: json["idpersona"],
    );

    Map<String, dynamic> toJson() => {
        "idusuario": idusuario,
        "login": login,
        "clave": clave,
        "correo": correo,
        "idpersona": idpersona,
    };
}
