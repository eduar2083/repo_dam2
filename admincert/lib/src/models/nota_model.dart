// To parse this JSON data, do
//
//     final notaModel = notaModelFromJson(jsonString);

import 'dart:convert';

List<NotaModel> notaModelFromJson(String str) => List<NotaModel>.from(json.decode(str).map((x) => NotaModel.fromJson(x)));

String notaModelToJson(List<NotaModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class NotaModel {
    NotaModel({
        this.idtipoevaluacion,
        this.idevaluacion,
        this.idnota,
        this.tipoevaluacion,
        this.nota,
        this.idpersona
    });

    int idtipoevaluacion;
    int idevaluacion;
    int idnota;
    String tipoevaluacion;
    double nota;

    // Added
    int idpersona;

    factory NotaModel.fromJson(Map<String, dynamic> json) => NotaModel(
        idtipoevaluacion: json["idtipoevaluacion"],
        idevaluacion: json["idevaluacion"],
        idnota: json["idnota"],
        tipoevaluacion: json["tipoevaluacion"],
        nota: json["nota"],
        idpersona: json["idpersona"]
    );

    Map<String, dynamic> toJson() => {
        "idtipoevaluacion": idtipoevaluacion,
        "idevaluacion": idevaluacion,
        "idnota": idnota,
        "tipoevaluacion": tipoevaluacion,
        "nota": nota,
        "idpersona": idpersona
    };
}
