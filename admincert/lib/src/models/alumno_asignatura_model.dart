// To parse this JSON data, do
//
//     final alumnoAsignaturaModel = alumnoAsignaturaModelFromJson(jsonString);

import 'dart:convert';

List<AlumnoAsignaturaModel> alumnoAsignaturaModelFromJson(String str) => List<AlumnoAsignaturaModel>.from(json.decode(str).map((x) => AlumnoAsignaturaModel.fromJson(x)));

String alumnoAsignaturaModelToJson(List<AlumnoAsignaturaModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AlumnoAsignaturaModel {
    AlumnoAsignaturaModel({
        this.idpersona,
        this.dni,
        this.nombre,
        this.apellido,
        this.direccion,
        this.telefono,
        this.fechaNacimiento,
        this.tipo,
    });

    int idpersona;
    dynamic dni;
    String nombre;
    String apellido;
    dynamic direccion;
    dynamic telefono;
    dynamic fechaNacimiento;
    dynamic tipo;

    factory AlumnoAsignaturaModel.fromJson(Map<String, dynamic> json) => AlumnoAsignaturaModel(
        idpersona: json["idpersona"],
        dni: json["dni"],
        nombre: json["nombre"],
        apellido: json["apellido"],
        direccion: json["direccion"],
        telefono: json["telefono"],
        fechaNacimiento: json["fechaNacimiento"],
        tipo: json["tipo"],
    );

    Map<String, dynamic> toJson() => {
        "idpersona": idpersona,
        "dni": dni,
        "nombre": nombre,
        "apellido": apellido,
        "direccion": direccion,
        "telefono": telefono,
        "fechaNacimiento": fechaNacimiento,
        "tipo": tipo,
    };
}
