// To parse this JSON data, do
//
//     final asignaturaPersonaModel = asignaturaPersonaModelFromJson(jsonString);

import 'dart:convert';

List<AsignaturaPersonaModel> asignaturaPersonaModelFromJson(String str) => List<AsignaturaPersonaModel>.from(json.decode(str).map((x) => AsignaturaPersonaModel.fromJson(x)));

String asignaturaPersonaModelToJson(List<AsignaturaPersonaModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AsignaturaPersonaModel {
    AsignaturaPersonaModel({
        this.idasignatura,
        this.idarea,
        this.area,
        this.asignatura,
    });

    int idasignatura;
    int idarea;
    dynamic area;
    String asignatura;

    factory AsignaturaPersonaModel.fromJson(Map<String, dynamic> json) => AsignaturaPersonaModel(
        idasignatura: json["idasignatura"],
        idarea: json["idarea"],
        area: json["area"],
        asignatura: json["asignatura"],
    );

    Map<String, dynamic> toJson() => {
        "idasignatura": idasignatura,
        "idarea": idarea,
        "area": area,
        "asignatura": asignatura,
    };
}
