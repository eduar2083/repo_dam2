import 'dart:async';

import 'package:admincert/src/blocs/validators.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';

class LoginBloc with Validators {
  final _usuarioController = BehaviorSubject<String>();
  final _contrasenaController = BehaviorSubject<String>();

  Stream<String> get usuarioStream => _usuarioController.stream.transform(validarUsuario);
  Stream<String> get contrasenaStream => _contrasenaController.stream.transform(validarContrasena);

  Stream<bool> get formValidStream => 
    Observable.combineLatest2(usuarioStream, contrasenaStream, (a, b) => true);

  Function(String) get changeUsuario => _usuarioController.sink.add;
  Function(String) get changeContrasena => _contrasenaController.sink.add;

  String get usuario => _usuarioController.value;
  String get contrasena => _contrasenaController.value;

  dispose() {
    _usuarioController?.close();
    _contrasenaController?.close();
  }
}