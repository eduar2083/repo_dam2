import 'dart:async';

class Validators {

  final validarUsuario = StreamTransformer<String, String>.fromHandlers(
    handleData: (usuario, sink) {
      if (usuario.length >= 3) {
        sink.add(usuario);
      }
      else {
        sink.addError('Debe tener mínimo 3 caracteres');
      }
    }
  );

  final validarContrasena = StreamTransformer<String, String>.fromHandlers(
    handleData: (contrasena, sink) {
      if (contrasena.length >= 3) {
        sink.add(contrasena);
      }
      else {
        sink.addError('Debe tener mínimo 3 caracteres');
      }
    }
  );
}