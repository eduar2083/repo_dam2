import 'package:http/http.dart' as http;
import 'package:admincert/src/models/asignatura_persona_model.dart';
import 'package:admincert/src/utils/utils.dart' as util;

class AsignaturaPersonaProvider {
  Future<List<AsignaturaPersonaModel>> listar(int idPersona) async {
    List<AsignaturaPersonaModel> asignaturas;

    final url = '${util.endPoint}/asignatura/persona/asignatura/$idPersona';

    final response = await http.get(url);
    final data = response.body;

    if (data != '') {
      asignaturas = asignaturaPersonaModelFromJson(data);
    }

    return asignaturas;
  }
}