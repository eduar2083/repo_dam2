import 'package:http/http.dart' as http;
import 'package:admincert/src/models/usuario_model.dart';
import 'package:admincert/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:admincert/src/providers/persona_provider.dart';
import 'package:admincert/src/providers/enlace_provider.dart';
import 'package:admincert/src/utils/utils.dart' as util;

class LoginProvider {

  Future<UsuarioModel> autenticarUsuario(String usuername, String contrasena) async {
    UsuarioModel usuario;

    final _prefs = new PreferenciasUsuario();

    final url = '${util.endPoint}/usuario/$usuername/$contrasena';

    final response = await http.get(url);
    final data = response.body;

    if (data != '') {
      usuario = usuarioModelFromJson(data);

      // Obtener datos de la persona
      final personaProvider = new PersonaProvider();
      final persona = await personaProvider.obtener(usuario.idpersona);

      // Obtener enlaces
      final EnlaceProvider enlaceProvider = new EnlaceProvider();
      final enlaces = await enlaceProvider.listar(usuario.idusuario);

      // Guardar en prefs
      _prefs.idUsuario = usuario.idusuario;
      _prefs.idPersona = usuario.idpersona;
      _prefs.login = usuario.login;
      _prefs.correo = usuario.correo;
      _prefs.nombre = persona.nombre;
      _prefs.apellido = persona.apellido;
      _prefs.dni = persona.dni;
      _prefs.direccion = persona.direccion;
      _prefs.telefono = persona.telefono;
      _prefs.fechaNacimiento = util.dateTimeToString(persona.fechaNacimiento, 'dd/MM/yyyy');
      _prefs.nombreCompleto = persona.nombre + ' ' + persona.apellido;

      List<String> listaEnlace = new List();
      for (var e in enlaces) {
        listaEnlace.add(e.idenlace.toString());
      }
      
      _prefs.listaEnlace = listaEnlace;
    }

    return usuario;
  }
}