import 'package:http/http.dart' as http;
import 'package:admincert/src/models/persona_model.dart';
import 'package:admincert/src/utils/utils.dart' as util;

class PersonaProvider {
  Future<PersonaModel> obtener(int idPersona) async {
    PersonaModel persona;

    final url = '${util.endPoint}/persona/$idPersona';

    final response = await http.get(url);
    final data = response.body;

    if (data != '') {
      persona = personaModelFromJson(data);
    }

    return persona;
  }
}