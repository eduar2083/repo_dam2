import 'package:http/http.dart' as http;
import 'package:admincert/src/utils/utils.dart' as util;
import 'package:admincert/src/models/nota_model.dart';

class NotaAsignaturaProvider {
  Future<List<NotaModel>> listar(int idAsignatura, int idUsuario) async {
    List<NotaModel> enlaces;

    final url = '${util.endPoint}/notas/asignatura/$idAsignatura/$idUsuario';

    final response = await http.get(url);
    final data = response.body;

    if (data != '') {
      enlaces = notaModelFromJson(data);
    }

    return enlaces;
  }
}