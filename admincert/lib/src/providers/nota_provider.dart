import 'dart:convert';

import 'package:admincert/src/dto/nota_dto.dart';
import 'package:http/http.dart' as http;
import 'package:admincert/src/utils/utils.dart' as util;
// import 'package:admincert/src/models/nota_model.dart';

class NotaAProvider {
  Future<bool> registrar(NotaDto nota) async {
    int r;

    final url = '${util.endPoint}/notas';

    final response = await http.post(
      url,
      headers: { "Content-Type": "application/json" },
      body: json.encode(nota.toJson())
    );
    final data = response.body;

    if (data != '') {
      r = int.parse(data);
    }

    return r != null && r > 0;
  }

  Future<bool> actualizar(NotaDto nota) async {
    int r;

    final url = '${util.endPoint}/notas';

    final response = await http.put(
      url,
      headers: { "Content-Type": "application/json" },
      body: json.encode(nota.toJson())
    );
    final data = response.body;

    if (data != '') {
      r = int.parse(data);
    }

    return r != null && r > 0;
  }
}