import 'package:http/http.dart' as http;
import 'package:admincert/src/utils/utils.dart' as util;
import 'package:admincert/src/models/alumno_asignatura_model.dart';

class AlumnoAsignaturaProvider {
  Future<List<AlumnoAsignaturaModel>> listar(int idAsignatura, int idDocente) async {
    List<AlumnoAsignaturaModel> alumnos;

    final url = '${util.endPoint}/persona/asignatura/$idAsignatura';

    final response = await http.get(url);
    final data = response.body;

    if (data != '') {
      alumnos = alumnoAsignaturaModelFromJson(data);
    }

    return alumnos;
  }
}