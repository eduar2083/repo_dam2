import 'package:admincert/src/models/enlace_model.dart';
import 'package:http/http.dart' as http;
import 'package:admincert/src/utils/utils.dart' as util;

class EnlaceProvider {
  Future<List<EnlaceModel>> listar(int idUsuario) async {
    List<EnlaceModel> enlaces;

    final url = '${util.endPoint}/enlace/$idUsuario';

    final response = await http.get(url);
    final data = response.body;

    if (data != '') {
      enlaces = enlaceModelFromJson(data);
    }

    return enlaces;
  }
}