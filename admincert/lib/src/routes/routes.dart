
import 'package:admincert/src/pages/lista_alumnos_page.dart';
import 'package:admincert/src/pages/lista_notas_registro_page.dart';
import 'package:flutter/widgets.dart';
import 'package:admincert/src/pages/lista_cursos_docente_page.dart';
import 'package:admincert/src/pages/lista_notas_page.dart';
import 'package:admincert/src/pages/login_page.dart';
import 'package:admincert/src/pages/lista_cursos_alumno_page.dart';
import 'package:admincert/src/pages/home_page.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => LoginPage(),
    'home': (BuildContext context) => HomePage(),
    'lista_cursos_alumno': (BuildContext context) => ListaCursosAlumnoPage(),
    'lista_notas': (BuildContext context) => ListaNotasPage(),
    'lista_cursos_docente': (BuildContext context) => ListaCursosDocentePage(),
    'lista_alumnos': (BuildContext context) => ListaAlumnoPage(),
    'lista_notas_registro': (BuildContext context) => ListaNotasRegistroPage()
  };
}
