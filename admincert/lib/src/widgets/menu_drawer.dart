import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:admincert/src/pages/home_page.dart';
import 'package:admincert/src/pages/lista_cursos_alumno_page.dart';
import 'package:admincert/src/pages/login_page.dart';
import 'package:admincert/src/preferencias_usuario/preferencias_usuario.dart';
import 'package:admincert/src/pages/lista_cursos_docente_page.dart';

class MenuDrawer extends StatelessWidget {
  final _prefs = new PreferenciasUsuario();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
          padding: EdgeInsets.zero, children: _obtenerOpciones(context)),
    );
  }

  List<Widget> _obtenerOpciones(BuildContext context) {
    List<Widget> opciones = new List();

    opciones.add(_crearHeader());
    opciones.add(_opcionDatosPersonales(context));
    for (var e in _opcionesDinamicas(context)) {
      opciones.add(e);
    }
    opciones.add(_opcionSalir(context));

    return opciones;
  }

  Widget _crearHeader() {
    return UserAccountsDrawerHeader(
      accountName: Text(_prefs.nombreCompleto),
      accountEmail: Text(_prefs.correo),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/header-drawer.jpg'),
              fit: BoxFit.cover)),
      currentAccountPicture: CircleAvatar(
        backgroundColor: Colors.white,
        child: Text(
          _prefs.nombre[0],
          style: TextStyle(fontSize: 40.0),
        ),
      ),
    );
  }

  Widget _opcionDatosPersonales(BuildContext context) {
    return ListTile(
      leading: Icon(Icons.assignment_ind, color: Colors.blue),
      title: Text('Datos personales'),
      onTap: () => Navigator.pushNamed(context, HomePage.routeName),
    );
  }

  Widget _opcionSalir(BuildContext context) {
    return ListTile(
      leading: Icon(Icons.exit_to_app, color: Colors.blue),
      title: Text('Salir'),
      onTap: () {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => LoginPage()),
            (Route<dynamic> route) => false);
      },
    );
  }

  List<Widget> _opcionesDinamicas(BuildContext context) {
    List<ListTile> lista = new List();
    List<String> opciones = _prefs.listaEnlace;
    ListTile opcion;

    for (var e in opciones) {
      switch (e) {
        case "6":
          opcion = ListTile(
            leading: Icon(
              Icons.assignment_turned_in,
              color: Colors.blue,
            ),
            title: Text('Ver mis notas'),
            onTap: () => Navigator.pushReplacementNamed(
                context, ListaCursosAlumnoPage.routeName),
          );
          break;
        case "1":
          opcion = ListTile(
            leading: Icon(
              Icons.mode_edit,
              color: Colors.blue,
            ),
            title: Text('Asignar notas'),
            onTap: () => Navigator.pushReplacementNamed(
                context, ListaCursosDocentePage.routeName),
          );
          break;
        default:
      }

      if (opcion != null) lista.add(opcion);
    }

    return lista;
  }
}
