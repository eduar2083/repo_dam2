import 'package:shared_preferences/shared_preferences.dart';

/*
  Recordar instalar el paquete de:
    shared_preferences:

  Inicializar en el main
    final prefs = new PreferenciasUsuario();
    await prefs.initPrefs();
    
    Recuerden que el main() debe de ser async {...

*/

class PreferenciasUsuario {

  static final PreferenciasUsuario _instancia = new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  // GET y SET del idusuario
  get idUsuario {
    return _prefs.getInt('idUsuario') ?? -1;
  }

  set idUsuario( int value ) {
    _prefs.setInt('idUsuario', value);
  }

  // GET y SET del idPersona
  get idPersona {
    return _prefs.getInt('idPersona') ?? -1;
  }

  set idPersona( int value ) {
    _prefs.setInt('idPersona', value);
  }

  // GET y SET del login
  get login {
    return _prefs.getString('login') ?? '';
  }

  set login( String value ) {
    _prefs.setString('login', value);
  }

  // GET y SET del correo
  get correo {
    return _prefs.getString('correo') ?? '';
  }

  set correo( String value ) {
    _prefs.setString('correo', value);
  }

  // GET y SET del dni
  get dni {
    return _prefs.getString('dni') ?? '';
  }

  set dni( String value ) {
    _prefs.setString('dni', value);
  }

  // GET y SET del nombre
  get nombre {
    return _prefs.getString('nombre') ?? '';
  }

  set nombre( String value ) {
    _prefs.setString('nombre', value);
  }

  // GET y SET del apellido
  get apellido {
    return _prefs.getString('apellido') ?? '';
  }

  set apellido( String value ) {
    _prefs.setString('apellido', value);
  }

  // GET y SET del apellido
  get nombreCompleto {
    return _prefs.getString('nombreCompleto') ?? '';
  }

  set nombreCompleto( String value ) {
    _prefs.setString('nombreCompleto', value);
  }

  // GET y SET del direccion
  get direccion {
    return _prefs.getString('direccion') ?? '';
  }

  set direccion( String value ) {
    _prefs.setString('direccion', value);
  }

  // GET y SET del telefono
  get telefono {
    return _prefs.getString('telefono') ?? '';
  }

  set telefono( String value ) {
    _prefs.setString('telefono', value);
  }

  // GET y SET del fechaNacimiento
  get fechaNacimiento {
    return _prefs.getString('fechaNacimiento') ?? '';
  }

  set fechaNacimiento( String value ) {
    _prefs.setString('fechaNacimiento', value);
  }


  // GET y SET de la lista enlaces
  get listaEnlace {
    return _prefs.getStringList('listaEnlace');
  }

  set listaEnlace( List<String> value ) {
    _prefs.setStringList('listaEnlace', value);
  }
}
